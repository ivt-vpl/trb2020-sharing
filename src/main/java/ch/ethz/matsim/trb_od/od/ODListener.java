package ch.ethz.matsim.trb_od.od;

import java.util.HashMap;
import java.util.Map;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.events.LinkEnterEvent;
import org.matsim.api.core.v01.events.PersonArrivalEvent;
import org.matsim.api.core.v01.events.PersonDepartureEvent;
import org.matsim.api.core.v01.events.handler.LinkEnterEventHandler;
import org.matsim.api.core.v01.events.handler.PersonArrivalEventHandler;
import org.matsim.api.core.v01.events.handler.PersonDepartureEventHandler;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.Person;
import org.matsim.vehicles.Vehicle;

public class ODListener implements PersonDepartureEventHandler, PersonArrivalEventHandler, LinkEnterEventHandler {
	private final Map<Id<Person>, PersonDepartureEvent> departureEvents = new HashMap<>();
	private final Map<Id<Vehicle>, Double> networkDistances = new HashMap<>();

	private final Network network;
	private final ODHandler handler;

	public ODListener(ODHandler handler, Network network) {
		this.handler = handler;
		this.network = network;
	}

	@Override
	public void handleEvent(PersonDepartureEvent departureEvent) {
		this.departureEvents.put(departureEvent.getPersonId(), departureEvent);
		this.networkDistances.put(Id.createVehicleId(departureEvent.getPersonId()), 0.0);
	}

	@Override
	public void handleEvent(PersonArrivalEvent arrivalEvent) {
		PersonDepartureEvent departureEvent = departureEvents.remove(arrivalEvent.getPersonId());
		Double networkDistance = networkDistances.remove(Id.createVehicleId(arrivalEvent.getPersonId()));

		if (departureEvent != null) {
			handler.handleOD(departureEvent, arrivalEvent, networkDistance);
		}
	}

	@Override
	public void handleEvent(LinkEnterEvent event) {
		Double currentValue = networkDistances.get(event.getVehicleId());

		if (currentValue != null) {
			networkDistances.put(event.getVehicleId(),
					currentValue + network.getLinks().get(event.getLinkId()).getLength());
		}
	}
}
