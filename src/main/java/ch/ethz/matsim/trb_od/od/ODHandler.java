package ch.ethz.matsim.trb_od.od;

import org.matsim.api.core.v01.events.PersonArrivalEvent;
import org.matsim.api.core.v01.events.PersonDepartureEvent;

public interface ODHandler {
	void handleOD(PersonDepartureEvent departureEvent, PersonArrivalEvent arrivalEvent, double networkDistance);
}
