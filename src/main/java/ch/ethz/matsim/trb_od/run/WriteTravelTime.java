package ch.ethz.matsim.trb_od.run;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.GZIPOutputStream;

import org.matsim.api.core.v01.network.Network;
import org.matsim.core.config.CommandLine;
import org.matsim.core.config.CommandLine.ConfigurationException;
import org.matsim.core.network.NetworkUtils;
import org.matsim.core.network.io.MatsimNetworkReader;

import ch.ethz.matsim.trb_od.travel_time.SerializableTravelTime;

public class WriteTravelTime {
	static public void main(String[] args) throws ConfigurationException, FileNotFoundException, IOException {
		CommandLine cmd = new CommandLine.Builder(args) //
				.requireOptions("network-path", "events-path", "output-path") //
				.allowOptions("start-time", "end-time", "interval") //
				.build();

		String networkPath = cmd.getOptionStrict("network-path");
		String eventsPath = cmd.getOptionStrict("events-path");
		String outputPath = cmd.getOptionStrict("output-path");

		double startTime = cmd.getOption("start-time").map(Double::parseDouble).orElse(5.0 * 3600.0);
		double endTime = cmd.getOption("end-time").map(Double::parseDouble).orElse(22.0 * 3600.0);
		double interval = cmd.getOption("interval").map(Double::parseDouble).orElse(300.0);

		Network network = NetworkUtils.createNetwork();
		new MatsimNetworkReader(network).readFile(networkPath);

		SerializableTravelTime travelTime = SerializableTravelTime.readFromEvents(startTime, endTime, interval, network,
				new File(eventsPath));

		OutputStream outputStream = new GZIPOutputStream(new FileOutputStream(new File(outputPath)));
		SerializableTravelTime.writeBinary(outputStream, travelTime);
		outputStream.close();
	}
}
