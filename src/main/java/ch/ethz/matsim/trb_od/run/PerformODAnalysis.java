package ch.ethz.matsim.trb_od.run;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.Polygon;
import org.matsim.api.core.v01.Coord;
import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.TransportMode;
import org.matsim.api.core.v01.events.PersonArrivalEvent;
import org.matsim.api.core.v01.events.PersonDepartureEvent;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.Person;
import org.matsim.core.api.experimental.events.EventsManager;
import org.matsim.core.config.CommandLine;
import org.matsim.core.config.CommandLine.ConfigurationException;
import org.matsim.core.events.EventsUtils;
import org.matsim.core.events.MatsimEventsReader;
import org.matsim.core.network.NetworkUtils;
import org.matsim.core.network.algorithms.TransportModeNetworkFilter;
import org.matsim.core.network.io.MatsimNetworkReader;
import org.matsim.core.router.AStarLandmarksFactory;
import org.matsim.core.router.costcalculators.OnlyTimeDependentTravelDisutility;
import org.matsim.core.router.util.LeastCostPathCalculator;
import org.matsim.core.router.util.LeastCostPathCalculator.Path;
import org.matsim.core.trafficmonitoring.FreeSpeedTravelTime;
import org.matsim.core.utils.geometry.geotools.MGC;
import org.matsim.core.utils.gis.PolygonFeatureFactory;
import org.matsim.core.utils.gis.ShapeFileWriter;
import org.matsim.core.utils.misc.Time;
import org.opengis.feature.simple.SimpleFeature;

import ch.ethz.matsim.trb_od.grid.HexagonGridFactory;
import ch.ethz.matsim.trb_od.od.ODHandler;
import ch.ethz.matsim.trb_od.od.ODListener;
import ch.ethz.matsim.trb_od.travel_time.SerializableTravelTime;

public class PerformODAnalysis {
	static public void main(String[] args)
			throws ConfigurationException, FileNotFoundException, IOException, InterruptedException {
		CommandLine cmd = new CommandLine.Builder(args) //
				.requireOptions("boundary-path", "network-path", "events-path", "od-output-path") //
				.allowOptions("hexagon-radius", "start-time", "end-time", "interval", "hexagon-output-path",
						"info-output-path") //
				.build();

		Geometry boundary = loadBoundary(cmd.getOptionStrict("boundary-path"));

		double hexagonRadius = cmd.getOption("hexagon-radius").map(Double::parseDouble).orElse(1000.0);
		List<Polygon> zones = new HexagonGridFactory(hexagonRadius).createGrid(boundary);

		Network fullNetwork = NetworkUtils.createNetwork();
		Network network = NetworkUtils.createNetwork();

		new MatsimNetworkReader(fullNetwork).readFile(cmd.getOptionStrict("network-path"));
		new TransportModeNetworkFilter(fullNetwork).filter(network, Collections.singleton("car"));

		Map<Id<Link>, Integer> linkToZone = new HashMap<>();
		List<List<Link>> zoneToLink = new ArrayList<>(zones.size());
		GeometryFactory geometryFactory = new GeometryFactory();

		for (int i = 0; i < zones.size(); i++) {
			zoneToLink.add(new LinkedList<>());
		}

		for (Link link : network.getLinks().values()) {
			for (int i = 0; i < zones.size(); i++) {
				Coordinate coordinate = new Coordinate(link.getCoord().getX(), link.getCoord().getY());
				Point point = geometryFactory.createPoint(coordinate);

				if (zones.get(i).contains(point)) {
					linkToZone.put(link.getId(), i);
					zoneToLink.get(i).add(link);
				}
			}
		}

		List<Link> centroidLinks = new ArrayList<>(zones.size());

		for (int i = 0; i < zones.size(); i++) {
			Coordinate coordinate = zones.get(i).getCentroid().getCoordinate();
			Link link = NetworkUtils.getNearestLink(network, new Coord(coordinate.getX(), coordinate.getY()));
			centroidLinks.add(link);
		}

		double startTime = Time.parseTime(cmd.getOption("start-time").orElse("08:00:00"));
		double endTime = Time.parseTime(cmd.getOption("end-time").orElse("22:00:00"));
		double interval = Time.parseTime(cmd.getOption("interval").orElse("00:15:00"));

		CustomODHandler odHandler = new CustomODHandler(startTime, endTime, interval, linkToZone, zones.size());
		ODListener odListener = new ODListener(odHandler, fullNetwork);

		int numberOfBins = 1 + (int) Math.floor((endTime - startTime) / interval);
		SerializableTravelTime.Listener travelTimeListener = new SerializableTravelTime.Listener(network, startTime,
				endTime, interval, numberOfBins);

		EventsManager eventsManager = EventsUtils.createEventsManager();
		eventsManager.addHandler(odListener);
		eventsManager.addHandler(travelTimeListener);
		new MatsimEventsReader(eventsManager).readFile(cmd.getOptionStrict("events-path"));

		FreeSpeedTravelTime freeSpeedTravelTime = new FreeSpeedTravelTime();
		SerializableTravelTime congestedTravelTime = new SerializableTravelTime(startTime, endTime, interval,
				numberOfBins, travelTimeListener.getData(), freeSpeedTravelTime);

		BufferedWriter writer = new BufferedWriter(
				new OutputStreamWriter(new FileOutputStream(cmd.getOptionStrict("od-output-path"))));

		writer.write(String.join(";", new String[] { //
				"start_time", "end_time", "origin_zone", "destination_zone", //
				"trips", "average_travel_time", "freespeed_travel_time", "congested_travel_time", "freespeed_distance",
				"congested_distance" }) + "\n");
		writer.flush();

		List<Thread> threads = new LinkedList<>();
		List<Task> tasks = new LinkedList<>();

		for (int slice = 0; slice < numberOfBins - 1; slice++) {
			for (int i = 0; i < zones.size(); i++) {
				for (int j = 0; j < zones.size(); j++) {
					tasks.add(new Task(i, j, slice));
				}
			}
		}

		long totalNumberOfTasks = tasks.size();
		AtomicLong processedNumberOfTasks = new AtomicLong(0);

		for (int k = 0; k < Runtime.getRuntime().availableProcessors(); k++) {
			Thread thread = new Thread(() -> {
				List<Task> localTasks = new LinkedList<>();

				LeastCostPathCalculator freeSpeedRouter = new AStarLandmarksFactory(
						Runtime.getRuntime().availableProcessors()).createPathCalculator(network,
								new OnlyTimeDependentTravelDisutility(freeSpeedTravelTime), freeSpeedTravelTime);

				LeastCostPathCalculator congestedRouter = new AStarLandmarksFactory(
						Runtime.getRuntime().availableProcessors()).createPathCalculator(network,
								new OnlyTimeDependentTravelDisutility(congestedTravelTime), congestedTravelTime);

				while (true) {
					localTasks.clear();

					synchronized (tasks) {
						while (tasks.size() > 0 && localTasks.size() < 10) {
							localTasks.add(tasks.remove(0));
						}

					}

					for (Task task : localTasks) {
						double averageTravelTime = odHandler.cumulativeTravelTimes[task.slice][task.i][task.j]
								/ odHandler.counts[task.slice][task.i][task.j];

						Link startLink = centroidLinks.get(task.i);
						Link endLink = centroidLinks.get(task.j);

						double time = task.slice * interval + startTime;
						double routeTime = time + 0.5 * interval;

						Path freespeedPath = freeSpeedRouter.calcLeastCostPath(startLink.getToNode(),
								endLink.getFromNode(), routeTime, null, null);
						Path congestedPath = congestedRouter.calcLeastCostPath(startLink.getToNode(),
								endLink.getFromNode(), routeTime, null, null);

						double freespeedDistance = calculateDistance(freespeedPath);
						double congestedDistance = calculateDistance(congestedPath);

						try {
							synchronized (writer) {
								writer.write(String.join(";", new String[] { //
										String.valueOf(time), String.valueOf(time + interval), //
										String.valueOf(task.i), String.valueOf(task.j), //
										String.valueOf(odHandler.counts[task.slice][task.i][task.j]), //
										String.valueOf(averageTravelTime), //
										String.valueOf(freespeedPath.travelTime), //
										String.valueOf(congestedPath.travelTime), //
										String.valueOf(freespeedDistance), //
										String.valueOf(congestedDistance) //
								}) + "\n");
								writer.flush();
							}
						} catch (IOException e) {
							e.printStackTrace();
							throw new RuntimeException(e);
						}
					}

					if (localTasks.size() == 0) {
						return;
					} else {
						processedNumberOfTasks.addAndGet(localTasks.size());
						System.out.println(String.format("%d / %d (%.2f%%)", processedNumberOfTasks.get(),
								totalNumberOfTasks, 100.0 * processedNumberOfTasks.get() / totalNumberOfTasks));
					}
				}
			});

			thread.start();
			threads.add(thread);
		}

		for (Thread thread : threads) {
			thread.join();
		}

		writer.close();

		if (cmd.hasOption("info-output-path")) {
			BufferedWriter infoWriter = new BufferedWriter(
					new OutputStreamWriter(new FileOutputStream(new File(cmd.getOptionStrict("info-output-path")))));
			infoWriter.write("Unique drivers: " + String.valueOf(odHandler.uniqueDrivers.size()) + "\n");
			infoWriter.write(
					"Total distance of unique drivers: " + String.valueOf(odHandler.totalNetworkDistance) + "\n");
			infoWriter.write("Total distance of unique drivers (time constrained): "
					+ String.valueOf(odHandler.totalNetworkDistanceSpecific) + "\n");
			infoWriter.flush();
			infoWriter.close();
		}

		if (cmd.hasOption("hexagon-output-path")) {
			Collection<SimpleFeature> features = new ArrayList<>(zones.size());

			PolygonFeatureFactory featureFactory = new PolygonFeatureFactory.Builder() //
					.setCrs(MGC.getCRS("EPSG:2056")) //
					.setName("zones") //
					.addAttribute("zone_index", Integer.class) //
					.create();

			for (int i = 0; i < zones.size(); i++) {
				SimpleFeature feature = featureFactory.createPolygon(zones.get(i), new Object[] { i }, null);
				features.add(feature);
			}

			ShapeFileWriter.writeGeometries(features, cmd.getOptionStrict("hexagon-output-path"));
		}
	}

	static public class Task {
		public final int i;
		public final int j;
		public final int slice;

		public Task(int i, int j, int slice) {
			this.i = i;
			this.j = j;
			this.slice = slice;
		}
	}

	static public class CustomODHandler implements ODHandler {
		public final double startTime;
		public final double endTime;
		public final double interval;
		public final int numberOfSlices;
		public final Map<Id<Link>, Integer> linkToZone;

		public final long counts[][][];
		public final double cumulativeTravelTimes[][][];
		public final Set<Id<Person>> uniqueDrivers = new HashSet<>();
		public double totalNetworkDistance = 0.0;
		public double totalNetworkDistanceSpecific = 0.0;

		public CustomODHandler(double startTime, double endTime, double interval, Map<Id<Link>, Integer> linkToZone,
				int numberOfZones) {
			this.startTime = startTime;
			this.endTime = endTime;
			this.interval = interval;
			this.numberOfSlices = (int) Math.ceil((endTime - startTime) / interval);
			this.linkToZone = linkToZone;

			this.counts = new long[numberOfSlices][numberOfZones][numberOfZones];
			this.cumulativeTravelTimes = new double[numberOfSlices][numberOfZones][numberOfZones];
		}

		@Override
		public void handleOD(PersonDepartureEvent departureEvent, PersonArrivalEvent arrivalEvent,
				double networkDistance) {
			if (departureEvent.getLegMode().equals(TransportMode.car)) {
				Integer originZone = linkToZone.get(departureEvent.getLinkId());
				Integer destinationZone = linkToZone.get(arrivalEvent.getLinkId());

				if (originZone != null && destinationZone != null) {
					if (departureEvent.getTime() >= startTime && arrivalEvent.getTime() < endTime) {
						int timeSlice = (int) ((departureEvent.getTime() - startTime) / interval);

						counts[timeSlice][originZone][destinationZone] += 1;
						cumulativeTravelTimes[timeSlice][originZone][destinationZone] += arrivalEvent.getTime()
								- departureEvent.getTime();
					}

					if (departureEvent.getTime() >= 57600.0 && departureEvent.getTime() < 65700.0) {
						totalNetworkDistanceSpecific += networkDistance;
					}

					uniqueDrivers.add(departureEvent.getPersonId());
					totalNetworkDistance += networkDistance;
				}
			}
		}
	}

	static private Geometry loadBoundary(String boundaryPath) {
		Geometry boundary = null;

		try {
			URL url = new File(boundaryPath).toURI().toURL();
			DataStore dataStore = DataStoreFinder.getDataStore(Collections.singletonMap("url", url));

			SimpleFeatureSource featureSource = dataStore.getFeatureSource(dataStore.getTypeNames()[0]);
			SimpleFeatureCollection featureCollection = featureSource.getFeatures();
			SimpleFeatureIterator featureIterator = featureCollection.features();

			while (featureIterator.hasNext()) {
				SimpleFeature feature = featureIterator.next();

				if (boundary != null) {
					throw new IllegalStateException("Found more than one shape in boundary file.");
				}

				boundary = (Geometry) feature.getDefaultGeometry();
			}

			featureIterator.close();
			dataStore.dispose();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		if (boundary == null) {
			throw new IllegalStateException("Did not find geometry in boundary file.");
		}

		return boundary;
	}

	static private double calculateDistance(Path path) {
		double distance = 0.0;

		for (Link link : path.links) {
			distance += link.getLength();
		}

		return distance;
	}
}
