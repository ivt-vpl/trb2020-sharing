package ch.ethz.matsim.trb_od.travel_time;

import org.matsim.api.core.v01.network.Link;
import org.matsim.core.mobsim.qsim.qnetsimengine.QVehicle;
import org.matsim.core.mobsim.qsim.qnetsimengine.linkspeedcalculator.LinkSpeedCalculator;
import org.matsim.core.router.util.TravelTime;

/**
 * This LinkSpeedCalculator draws from a TravelTime object to provide travel
 * times for links in the network simulation.
 * 
 * @author Sebastian Hörl <sebastian.hoerl@ivt.baug.ethz.ch>
 */
public class TravelTimeBasedLinkSpeedCalculator implements LinkSpeedCalculator {
	private final TravelTime travelTime;

	public TravelTimeBasedLinkSpeedCalculator(TravelTime travelTime) {
		this.travelTime = travelTime;
	}

	@Override
	public double getMaximumVelocity(QVehicle vehicle, Link link, double time) {
		return travelTime.getLinkTravelTime(link, time, null, null);
	}
}
