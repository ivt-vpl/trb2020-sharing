package ch.ethz.matsim.trb_od.grid;

import java.util.LinkedList;
import java.util.List;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Polygon;

public class HexagonGridFactory implements GridFactory {
	private final GeometryFactory geometryFactory = new GeometryFactory();

	private final double radius;

	private final double offsetX;
	private final double offsetY;

	public HexagonGridFactory(double radius) {
		this.radius = radius;

		offsetX = 2.0 * radius * Math.cos(30.0 * Math.PI / 180.0);
		offsetY = radius + 0.5 * offsetX / Math.sqrt(3.0);
	}

	public List<Polygon> createGrid(Geometry geometry) {
		Geometry envelope = geometry.getEnvelope();
		Coordinate[] bounds = envelope.getCoordinates();

		double minX = bounds[0].getX();
		double minY = bounds[0].getY();
		double maxX = bounds[2].getX();
		double maxY = bounds[2].getY();

		int maxI = (int) Math.ceil((maxX - minX) / offsetX) + 1;
		int maxJ = (int) Math.ceil((maxY - minY) / offsetY) + 1;

		List<Polygon> hexagons = new LinkedList<>();

		for (int i = 0; i < maxI; i++) {
			for (int j = 0; j < maxJ; j++) {
				double centroidX = minX + offsetX * i;

				if (j % 2 == 0) {
					centroidX += 0.5 * offsetX;
				}

				double centroidY = minY + offsetY * j;

				Coordinate[] vertices = new Coordinate[7];

				for (int k = 0; k < 7; k++) {
					double angle = Math.PI * (30.0 + k * 60.0) / 180.0;
					vertices[k] = new Coordinate(Math.cos(angle) * radius + centroidX,
							Math.sin(angle) * radius + centroidY);
				}

				Polygon polygon = geometryFactory.createPolygon(vertices);
				
				if (geometry.intersects(polygon)) {
					hexagons.add(polygon);
				}
			}
		}

		return hexagons;
	}
}
