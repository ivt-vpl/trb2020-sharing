package ch.ethz.matsim.trb_od.grid;

import java.util.List;

import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.Polygon;

public interface GridFactory {
	List<Polygon> createGrid(Geometry geometry);
}
