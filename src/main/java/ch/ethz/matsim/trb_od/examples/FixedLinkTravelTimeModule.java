package ch.ethz.matsim.trb_od.examples;

import org.matsim.api.core.v01.Scenario;
import org.matsim.core.api.experimental.events.EventsManager;
import org.matsim.core.controler.AbstractModule;
import org.matsim.core.mobsim.qsim.qnetsimengine.ConfigurableQNetworkFactory;
import org.matsim.core.mobsim.qsim.qnetsimengine.QNetworkFactory;
import org.matsim.core.router.util.TravelTime;

import com.google.inject.Provides;
import com.google.inject.Singleton;

import ch.ethz.matsim.trb_od.travel_time.TravelTimeBasedLinkSpeedCalculator;

public class FixedLinkTravelTimeModule extends AbstractModule {
	private final TravelTime travelTime;

	public FixedLinkTravelTimeModule(TravelTime travelTime) {
		this.travelTime = travelTime;
	}

	@Override
	public void install() {
		bind(QNetworkFactory.class).to(ConfigurableQNetworkFactory.class);
	}

	@Provides
	@Singleton
	public TravelTimeBasedLinkSpeedCalculator provideTravelTimeLinkSpeedCalculator() {
		return new TravelTimeBasedLinkSpeedCalculator(travelTime);
	}

	@Provides
	@Singleton
	public ConfigurableQNetworkFactory provideConfigurableQNetworkFactory(EventsManager eventsManager,
			Scenario scenario, TravelTimeBasedLinkSpeedCalculator linkSpeedCalculator) {
		ConfigurableQNetworkFactory factory = new ConfigurableQNetworkFactory(eventsManager, scenario);
		factory.setLinkSpeedCalculator(linkSpeedCalculator);
		return factory;
	}

}
